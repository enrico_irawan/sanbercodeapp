import React, { useContext } from 'react'
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, FlatList } from 'react-native'
import List from './List'
import {RootContext} from './index'

const TodoList = () => {
    const state = useContext(RootContext);
    console.log(state);

    return (
        <View style={styles.screen}>
            <Text>Masukan Todolist</Text>
            <View style={styles.form}>
                <TextInput
                    style={styles.input}
                    placeholder="Input here"
                    value={state.title}
                    onChangeText={(value) => state.handleChangeInput(value)}
                />
                <TouchableOpacity style={styles.button} onPress={state.onPressHandler}>
                    <Text style={{ color: 'white' }}>+</Text>
                </TouchableOpacity>
            </View>
            <View style={{ height: 20 }} />
            <FlatList
                data={state.data}
                // keyExtractor={state.item => item.id}
                renderItem={({ item }) => {
                    return <List data={item} id={item.id} onDelete={() => state.onDelete(item.id)} />
                }}
                ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
            />
        </View>
    )
}

export default TodoList

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10
    },
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    input: {
        width: '80%',
        height: 50,
        borderWidth: 1,
        marginRight: 10
    },
    button: {
        backgroundColor: 'blue',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
