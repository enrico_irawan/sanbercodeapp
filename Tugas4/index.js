import React, {createContext, useState} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
    const [data, setData] = useState([]);
    const [title, setTitle] = useState('');

    const handleChangeInput = val => {
        setTitle(val);
    }

    const onPressHandler = () => {
        setData([
            ...data,
            {
                date: new Date().toLocaleDateString(),
                id: Math.random().toString(),
                title: title
            }
        ])
        setTitle('');
    }

    const onDelete = (id) => {
        setData(data => {
            return data.filter(list => list.id !== id)
        })
    }

    return(
        <RootContext.Provider value={
            {data, title, onPressHandler, onDelete, handleChangeInput}
        }>
            <TodoList />
        </RootContext.Provider>
    )
}

export default Context;