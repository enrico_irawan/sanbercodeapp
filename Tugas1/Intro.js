import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Intro = () => {
  return (
    <View style={styles.container}>
      <Text>Halo Kelas React Native Lanjutan Sanbercode</Text>
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
