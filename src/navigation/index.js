import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import SplashScreen from "../screen/Splash";
import Intro from "../screen/Intro";
import Login from "../screen/Login";
import Home from "../screen/Home";
import Profile from "../screen/Profile";

import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Register from "../screen/Register";
import Maps from "../screen/maps";
import ReactNative from "../screen/React-Native";
import Chat from "../screen/Chat";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainNavigation = () => (
  <Tab.Navigator>
    <Tab.Screen
      component={Home}
      name="Home"
      options={{
        tabBarLabel: "Home",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="home" color={color} size={size} />
        ),
      }}
    />
    <Tab.Screen
      component={Maps}
      name="Maps"
      options={{
        tabBarLabel: "Maps",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons
            name="google-maps"
            color={color}
            size={size}
          />
        ),
      }}
    />
    <Tab.Screen
      component={Chat}
      name="Chat"
      options={{
        tabBarLabel: "Chat",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons
            name="chat-outline"
            color={color}
            size={size}
          />
        ),
      }}
    />
    <Tab.Screen
      component={Profile}
      name="Profile"
      options={{
        tabBarLabel: "Profile",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons
            name="account-circle"
            color={color}
            size={size}
          />
        ),
      }}
    />
  </Tab.Navigator>
);

const StackNavigation = () => (
  <Stack.Navigator initialRouteName="Splash">
    <Stack.Screen
      name="Splash"
      component={SplashScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="Main"
      component={MainNavigation}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="React"
      component={ReactNative}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);

const AppNavigation = () => {
  return <StackNavigation />;
};

export default AppNavigation;
