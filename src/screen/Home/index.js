import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { ScrollView } from "react-native-gesture-handler";

const Home = ({ navigation }) => {
  return (
    <View style={styles.screen}>
      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Text style={styles.headerText}>Kelas</Text>
        </View>
        <View style={styles.cardContent}>
          <View style={styles.content}>
            <TouchableOpacity onPress={() => navigation.push("React")}>
              <MaterialCommunityIcons name="react" size={60} color="white" />
              <Text style={styles.contentText}>React Native</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.content}>
            <MaterialCommunityIcons
              name="language-python"
              size={60}
              color="white"
            />
            <Text style={styles.contentText}>Python</Text>
          </View>
          <View style={styles.content}>
            <MaterialCommunityIcons name="react" size={60} color="white" />
            <Text style={styles.contentText}>React JS</Text>
          </View>
          <View style={styles.content}>
            <MaterialCommunityIcons name="laravel" size={60} color="white" />
            <Text style={styles.contentText}>Laravel</Text>
          </View>
        </View>
      </View>

      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Text style={styles.headerText}>Kelas</Text>
        </View>
        <View style={styles.cardContent}>
          <View style={styles.content}>
            <MaterialCommunityIcons name="wordpress" size={60} color="white" />
            <Text style={styles.contentText}>Wordpress</Text>
          </View>
          <View style={styles.content}>
            <Image
              source={require("../../assets/images/website-design.png")}
              style={styles.image}
            />
            <Text style={styles.contentText}>Design Grafis</Text>
          </View>
          <View style={styles.content}>
            <MaterialCommunityIcons name="server" size={60} color="white" />
            <Text style={styles.contentText}>Web Server</Text>
          </View>
          <View style={styles.content}>
            <Image
              source={require("../../assets/images/ux.png")}
              style={styles.image}
            />
            <Text style={styles.contentText}>UI/UX Design</Text>
          </View>
        </View>
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.cardSummary}>
          <View style={styles.summaryHeader}>
            <Text style={styles.summaryHeaderText}>Summary</Text>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>React Native</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>20 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>Data Science</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>30 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>React JS</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>66 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>Laravel</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>60 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>Wordpress</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>30 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>Graphic Design</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>50 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>Web Server</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>25 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryContentTitle}>
            <Text style={styles.summaryContentText}>UI/UX Design</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Today</Text>
              <Text style={styles.contentText}>10 orang</Text>
            </View>
            <View style={styles.summaryContentTextWrapper}>
              <Text style={styles.contentText}>Total</Text>
              <Text style={styles.contentText}>100 orang</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    padding: 15,
    backgroundColor: "white",
    justifyContent: "flex-start",
    marginBottom: "-4%",
  },
  card: {
    width: "100%",
    height: "20%",
    borderRadius: 10,
    overflow: "hidden",
    elevation: 6,
    marginBottom: "4%",
  },
  cardHeader: {
    width: "100%",
    height: "20%",
    backgroundColor: "#088dc4",
    padding: 5,
  },
  headerText: {
    color: "white",
  },
  cardContent: {
    flexDirection: "row",
    height: "100%",
    padding: 10,
    justifyContent: "space-around",
    backgroundColor: "#3EC6FF",
  },
  content: {
    alignItems: "center",
  },
  contentText: {
    color: "white",
    fontSize: 12,
  },
  image: {
    width: "100%",
    height: "58%",
  },
  cardSummary: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
    overflow: "hidden",
    elevation: 6,
  },
  summaryHeader: {
    backgroundColor: "#088dc4",
  },
  summaryHeaderText: {
    color: "white",
    padding: "2%",
  },
  summaryContentTitle: {
    backgroundColor: "#3EC6FF",
    padding: "2%",
  },
  summaryContentText: {
    color: "white",
  },
  summaryContent: {
    padding: "2%",
    backgroundColor: "#088dc4",
  },
  summaryContentTextWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: "10%",
  },
});
