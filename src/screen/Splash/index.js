import React, { useEffect } from "react";
import { View, Image, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

function SplashScreen({ navigation }) {
  const getUserToken = async () => {
    const response = await AsyncStorage.getItem("token");
    console.log(response);
    if (response === null) {
      return false;
    } else {
      return true;
    }
  };
  const getIntroToken = async () => {
    const response = await AsyncStorage.getItem("intro_token");
    return response;
  };

  useEffect(() => {
    const userToken = getUserToken();
    const introToken = getIntroToken();

    setTimeout(() => {
      if (userToken) {
        navigation.replace("Main");
      } else if (introToken) {
        navigation.replace("Login");
      } else {
        navigation.replace("Intro");
      }
    }, 2000);

    () => getUserToken();
    () => getIntroToken();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require("../../assets/images/logo.jpg")} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffffff",
  },
  logoContainer: {
    padding: 20,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default SplashScreen;
