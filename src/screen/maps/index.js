import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";

MapboxGL.setAccessToken(
  "pk.eyJ1IjoiZW5yaWNvaXJhd2FuIiwiYSI6ImNrZXR3NGNyczF6bXIyeGxna3hzbjRrZGsifQ.MNVW6Mk8D0CUAg7wQkZ64w"
);

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

const Maps = () => {
  const getLocation = async () => {
    try {
      const permission = await MapboxGL.requestAndroidLocationPermissions();
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    getLocation();
  }, []);

  const RenderPointAnnotation = ({ longitude, latidude }) => {
    let coordinate = [longitude, latidude];
    return (
      <MapboxGL.PointAnnotation id="pointAnotation" coordinate={coordinate}>
        <MapboxGL.Callout
          title={`Longitude: ${longitude}, Latidude: ${latidude}`}
        />
      </MapboxGL.PointAnnotation>
    );
  };

  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={{ flex: 1 }}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((item, idx) => {
          return (
            <RenderPointAnnotation
              key={idx.toString()}
              longitude={item[0]}
              latidude={item[1]}
            />
          );
        })}
      </MapboxGL.MapView>
    </View>
  );
};

export default Maps;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
