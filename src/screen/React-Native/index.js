import React, { useState } from "react";
import { StyleSheet, Text, View, processColor } from "react-native";
import { BarChart } from "react-native-charts-wrapper";

const ReactNative = () => {
  const arrayData = [
    [100, 40],
    [80, 60],
    [40, 90],
    [78, 45],
    [67, 87],
    [98, 32],
    [150, 90],
  ];

  const convertData = (data) => {
    const dataChart = [];
    data.forEach((item) => {
      return dataChart.push({
        y: [item[0], item[1]],
        marker: [
          `React Native Dasar ${item[0]}`,
          `React Native Lanjutan ${item[1]}`,
        ],
      });
    });
    return dataChart;
  };

  // const dataChart = [
  //   { y: [100, 40], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [80, 60], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [40, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [78, 45], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [67, 87], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [98, 32], marker: ["React Native Dasar", "React Native Lanjutan"] },
  //   { y: [150, 90], marker: ["React Native Dasar", "React Native Lanjutan"] },
  // ];

  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          values: convertData(arrayData),
          label: "",
          config: {
            colors: [processColor("#00bfff"), processColor("#06a2d6")],
            stackLabels: ["React Native Dasar", "React Native Lanjutan"],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: "SQUARE",
    formSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });

  const [xAxis, setXAxis] = useState({
    valueFormatter: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Agu",
      "Sep",
      "Oct",
      "Nov",
      "Des",
    ],
    position: "BOTTOM",
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{ limit: 115, lineColor: processColor("red"), lineWidth: 1 }],
  });

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <View style={styles.container}>
      <BarChart
        style={{ flex: 1 }}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        chartDescription={{ text: "" }}
        legend={legend}
        doubleTapToZoomEnabled={false}
        pinchZoom={false}
        marker={{ enabled: true }}
      />
    </View>
  );
};

export default ReactNative;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
  },
});
