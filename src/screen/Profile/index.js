import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { TouchableOpacity } from "react-native-gesture-handler";
import { GoogleSignin } from "@react-native-community/google-signin";

const Profile = ({ navigation }) => {
  const [user, setUser] = useState(null);
  const [isGoogleSignIn, setIsGoogleSignIn] = useState(false);

  const getItem = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      console.log("token user", value);
    } catch (e) {
      console.log(e);
    }
  };

  const removeData = async () => {
    try {
      await AsyncStorage.removeItem("token");
      console.log("Remove user token success");
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getItem();
    getCurrentUser();
    getCurrentUserInfo();
    googleSignIn();
  }, []);

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      setUser(userInfo);
      console.log("Goole User info ->", userInfo);
      console.log("state user ->", user);
    } catch (error) {
      console.log(error);
    }
  };

  const googleSignIn = async () => {
    const response = await GoogleSignin.isSignedIn();
    // console.log("isGoogleSignIn", response);
    setIsGoogleSignIn(response);
  };

  const getCurrentUserInfo = async () => {
    const currentUser = await GoogleSignin.getCurrentUser();
    console.log("current user ->", currentUser);
  };

  const onLogoutHandler = async () => {
    try {
      if (!isGoogleSignIn) {
        await removeData();
        navigation.reset({
          index: 0,
          routes: [{ name: "Login" }],
        });
      } else {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        navigation.replace("Login");
      }
    } catch (error) {}
  };

  return (
    <View style={styles.screen}>
      <View style={styles.background1}>
        <Image
          source={{ uri: user && user.user && user.user.photo }}
          style={styles.image}
        />
        <Text style={styles.text}>{user && user.user && user.user.name}</Text>
      </View>
      <View style={styles.background2}>
        <View style={styles.card}>
          <View style={styles.content}>
            <Text>Tanggal Lahir</Text>
            <Text>2 Agustus 2000</Text>
          </View>
          <View style={styles.content}>
            <Text>Jenis Kelamin</Text>
            <Text>Laki-laki</Text>
          </View>
          <View style={styles.content}>
            <Text>Hobi</Text>
            <Text>Ngoding</Text>
          </View>
          <View style={styles.content}>
            <Text>No Telepon</Text>
            <Text>089507764888</Text>
          </View>
          <View style={styles.content}>
            <Text>Email</Text>
            <Text>{user && user.user && user.user.email}</Text>
          </View>
          <View style={styles.buttonContent}>
            <TouchableOpacity
              style={styles.logoutButton}
              onPress={onLogoutHandler}
            >
              <Text style={styles.logoutButtonText}>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  background1: {
    backgroundColor: "#3EC6FF",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  background2: {
    backgroundColor: "white",
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: "center",
  },
  text: {
    color: "white",
    marginTop: 10,
    fontSize: 16,
  },
  card: {
    padding: 20,
    elevation: 2,
    width: "80%",
    height: "70%",
    borderRadius: 10,
    marginTop: "-45%",
    backgroundColor: "white",
    justifyContent: "center",
  },
  content: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,
  },
  buttonContent: {
    justifyContent: "center",
  },
  logoutButton: {
    backgroundColor: "#3EC6FF",
    width: "100%",
    height: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  logoutButtonText: {
    color: "white",
  },
});
