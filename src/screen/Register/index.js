import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  Alert,
  Modal,
  StatusBar,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { RNCamera } from "react-native-camera";
import Icon from "react-native-vector-icons/Feather";
import MaterialCommunity from "react-native-vector-icons/MaterialCommunityIcons";
import storage from "@react-native-firebase/storage";

function Register({ navigation }) {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState("back");
  const [photo, setPhoto] = useState(null);

  const toggleCamera = () => {
    setType(type === "back" ? "front" : "back");
  };

  const takePicture = async () => {
    const options = { quality: 0.5, base64: true };
    if (camera) {
      const data = await camera.takePictureAsync(options);
      setPhoto(data.uri);
      console.log("photo -> ", photo);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((res) => alert("Upload Success"))
      .catch((e) => alert("error"));
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{ flex: 1 }}>
          <RNCamera
            style={{ flex: 1 }}
            type={type}
            ref={(ref) => {
              camera = ref;
            }}
          >
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity style={styles.btnFlip} onPress={toggleCamera}>
                <MaterialCommunity
                  name="rotate-3d-variant"
                  size={30}
                  color="white"
                />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.buttonTakeContainer}>
              <TouchableOpacity style={styles.buttonTake} onPress={takePicture}>
                <Icon name="camera" size={50} color="white" />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <View style={styles.screen}>
        <View style={styles.background1}>
          <TouchableOpacity onPress={() => setIsVisible(true)}>
            <Image
              source={
                photo === null
                  ? require("../../assets/images/photo.jpg")
                  : { uri: photo }
              }
              style={styles.image}
            />
            <Text style={styles.text}>Change Picture</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.background2}>
          <View style={styles.card}>
            <View style={styles.content}>
              <Text>Nama</Text>
              <TextInput style={styles.input} placeholder="Nama" />
            </View>
            <View style={styles.content}>
              <Text>Email</Text>
              <TextInput style={styles.input} placeholder="Email" />
            </View>
            <View style={styles.content}>
              <Text>Password</Text>
              <TextInput style={styles.input} placeholder="Password" />
            </View>
            <View style={styles.buttonContent}>
              <TouchableOpacity
                style={styles.logoutButton}
                onPress={() => uploadImage(photo)}
              >
                <Text style={styles.logoutButtonText}>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      {renderCamera()}
    </>
  );
}

export default Register;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  background1: {
    backgroundColor: "#3EC6FF",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  background2: {
    backgroundColor: "white",
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: "center",
  },
  text: {
    color: "white",
    marginTop: 10,
    fontSize: 16,
  },
  card: {
    padding: 20,
    elevation: 2,
    width: "80%",
    height: "70%",
    borderRadius: 10,
    marginTop: "-60%",
    backgroundColor: "white",
    justifyContent: "center",
  },
  content: {
    marginVertical: 10,
  },
  buttonContent: {
    justifyContent: "center",
  },
  logoutButton: {
    backgroundColor: "#3EC6FF",
    width: "100%",
    height: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  logoutButtonText: {
    color: "white",
  },
  input: {
    width: "100%",
    borderBottomWidth: 1,
    borderBottomColor: "grey",
  },
  btnFlip: {
    padding: 15,
  },
  buttonTakeContainer: {
    alignItems: "center",
    justifyContent: "flex-end",
    flex: 2,
  },
  buttonTake: {
    marginBottom: 50,
  },
  round: {
    borderColor: "white",
    borderWidth: 2,
    width: 200,
    height: 300,
    borderRadius: 100,
    alignSelf: "center",
  },
});
