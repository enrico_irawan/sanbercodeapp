import React, { useState, useEffect } from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { TextInput, TouchableOpacity } from "react-native-gesture-handler";

import api from "../../api";

import Axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";
import TouchID from "react-native-touch-id";

import auth from "@react-native-firebase/auth";

import {
  GoogleSignin,
  GoogleSigninButton,
} from "@react-native-community/google-signin";

const config = {
  title: "Authentication Required",
  imageColor: "#191970",
  imageErrorColor: "red",
  sensorDescription: "Touch sensor",
  sensorErrorDescription: "Failed",
  cancelText: "Cancel",
};

function Login({ navigation }) {
  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem("token", token);
    } catch (e) {
      console.log(error);
    }
  };

  const onChangeText = (key, value) => {
    setData({
      ...data,
      [key]: value,
    });
  };

  const onLoginHandler = async () => {
    const response = Axios.post(`${api}\login`, data, {
      timeout: 20000,
    });
    try {
      const responseData = await response;
      await saveToken(responseData.data.token);
      navigation.replace("Main");
    } catch (error) {
      console.log(error);
    }
  };

  // const onLoginHandler = async () => {
  //   try {
  //     await auth().signInWithEmailAndPassword(data.email, data.password);
  //     navigation.replace("Main");
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        "547202859141-jp9ev1q0k4g6lm7s6rh629cuaq52pk13.apps.googleusercontent.com",
      offlineAccess: false,
    });
  };

  const onSignInWithGoogleHandler = async () => {
    try {
      const { idToken } = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      await saveToken(idToken);
      navigation.replace("Main");
    } catch (error) {}
  };

  const signInWithFingerprint = async () => {
    try {
      await TouchID.authenticate("", config);
      navigation.replace("Main");
    } catch (error) {
      console.log(error);
      alert("Authentikasi gagal");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={require("../../assets/images/logo.jpg")} />
      </View>
      <View style={styles.formContainer}>
        <Text>Username</Text>
        <TextInput
          style={styles.input}
          placeholder="Username atau email"
          value={data.email}
          onChangeText={(value) => onChangeText("email", value)}
        />
        <Text>Password</Text>
        <TextInput
          secureTextEntry
          style={styles.input}
          placeholder="Password"
          value={data.password}
          onChangeText={(value) => onChangeText("password", value)}
        />
        <TouchableOpacity style={styles.buttonLogin} onPress={onLoginHandler}>
          <Text style={styles.buttonTextLogin}>LOGIN</Text>
        </TouchableOpacity>
        <View style={{ alignSelf: "center", marginTop: 10 }}>
          <Text>OR</Text>
        </View>
        <GoogleSigninButton
          style={{ width: "100%", height: 48 }}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={onSignInWithGoogleHandler}
        />
        <TouchableOpacity
          style={styles.buttonFingerprint}
          onPress={signInWithFingerprint}
        >
          <Text style={styles.buttonTextLogin}>SIGN IN WITH FINGERPRINT</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity onPress={() => navigation.push("Register")}>
          <Text>Belum Mempunyai Akun? Register disini</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    justifyContent: "center",
    flex: 1,
    padding: 20,
    alignItems: "center",
  },
  logoContainer: {
    alignSelf: "center",
  },
  formContainer: {
    justifyContent: "flex-start",
    width: "100%",
  },
  input: {
    borderBottomColor: "grey",
    width: "100%",
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  buttonLogin: {
    width: "100%",
    backgroundColor: "#3EC6FF",
    height: 35,
    borderRadius: 4,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonTextLogin: {
    color: "white",
  },
  footer: {
    alignItems: "flex-end",
    justifyContent: "flex-end",
    flex: 1,
  },
  createAccount: {
    color: "blue",
  },
  buttonFingerprint: {
    width: "100%",
    backgroundColor: "#191970",
    height: 35,
    borderRadius: 4,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 3,
  },
});
