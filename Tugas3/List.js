import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const List = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.text}>
                <Text>{props.data.date}</Text>
                <Text>{props.data.title}</Text>
            </View>
            <View style={styles.icon}>
                <TouchableOpacity onPress={() => props.onDelete(props.id)}>
                    <Text style={{ fontSize: 20 }}>X</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default List

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: 'grey',
        borderWidth: 3,
        padding: 10
    },
    text: {
        justifyContent: 'flex-start',
        alignItems: 'flex-end'
    },
    icon: {}
})
