import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, FlatList } from 'react-native'
import List from './List'

const TodoList = () => {
    const [data, setData] = useState([]);
    const [title, setTitle] = useState('');

    const onPressHandler = () => {
        setData([
            ...data,
            {
                date: new Date().toLocaleDateString(),
                id: Math.random().toString(),
                title: title
            }
        ])
        setTitle('');
    }

    const onDelete = (id) => {
        setData(data => {
            return data.filter(list => list.id !== id)
        })
    }

    return (
        <View style={styles.screen}>
            <Text>Masukan Todolist</Text>
            <View style={styles.form}>
                <TextInput 
                style={styles.input} 
                placeholder="Input here"
                value={title} 
                onChangeText={(value) => setTitle(value)} 
                />
                <TouchableOpacity style={styles.button} onPress={onPressHandler}>
                    <Text style={{color: 'white'}}>+</Text>
                </TouchableOpacity>
            </View>
            <View style={{height: 20}} />
            <FlatList
                data={data}
                keyExtractor={item => item.id}
                renderItem={({ item }) => {
                    return <List data={item} id={item.id} onDelete={onDelete} />
                }}
                ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
            />
        </View>
    )
}

export default TodoList

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10
    },
    form: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    input: {
        width: '80%',
        height: 50,
        borderWidth: 1,
        marginRight: 10
    },
    button: {
       backgroundColor: 'blue',
       width: 50,
       height: 50,
       alignItems: 'center',
       justifyContent: 'center'
    }
});
