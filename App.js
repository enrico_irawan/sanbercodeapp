import React from "react";
import AppNavigation from "./src/navigation";
import firebase from "@react-native-firebase/app";
import { NavigationContainer } from "@react-navigation/native";

var firebaseConfig = {
  apiKey: "AIzaSyBFvYoEo2_VDNW2LHaMRE5CV40kddpw0mI",
  authDomain: "sanbercode-b145c.firebaseapp.com",
  databaseURL: "https://sanbercode-b145c.firebaseio.com",
  projectId: "sanbercode-b145c",
  storageBucket: "sanbercode-b145c.appspot.com",
  messagingSenderId: "547202859141",
  appId: "1:547202859141:web:c6ca21cbcb49e9bebd29ec",
  measurementId: "G-3WNY2K9PTB",
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return (
    <>
      <NavigationContainer>
        <AppNavigation />
      </NavigationContainer>
    </>
  );
};

export default App;
