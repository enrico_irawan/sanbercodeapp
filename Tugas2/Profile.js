import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

const Profile = () => {
    return (
        <View style={styles.screen}>
            <View style={styles.background1}>
                <Image source={require('./assets/photo.jpg')} style={styles.image} />
                <Text style={styles.text}>Enrico Irawan</Text>
            </View>
            <View style={styles.background2}>
                <View style={styles.card}>
                    <View style={styles.content}>
                        <Text>Tanggal Lahir</Text>
                        <Text>2 Agustus 2000</Text>
                    </View>
                    <View style={styles.content}>
                        <Text>Jenis Kelamin</Text>
                        <Text>Lkai-laki</Text>
                    </View>
                    <View style={styles.content}>
                        <Text>Hobi</Text>
                        <Text>Ngoding</Text>
                    </View>
                    <View style={styles.content}>
                        <Text>No Telepon</Text>
                        <Text>089507764888</Text>
                    </View>
                    <View style={styles.content}>
                        <Text>Email</Text>
                        <Text>enricoeusebius@gmail.com</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    background1: {
        backgroundColor: '#3EC6FF',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    background2: {
        backgroundColor: 'white',
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 50,
        alignSelf: 'center'
    },
    text: {
        color: 'white',
        marginTop: 10,
        fontSize: 16
    },
    card: {
        padding: 20,
        elevation: 2,
        width: '80%',
        height: '55%',
        borderRadius: 10,
        marginTop: '-70%',
        backgroundColor: 'white'
    },
    content: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10
    }
})
